import 'package:flutter/material.dart';
import 'Models/movieModel.dart';
import 'package:http/http.dart'
    as http; //para usar el modelo a partir de la API
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart'; //Carousel

/*  API URL's */
const baseUrl =
    "https://api.themoviedb.org/3/movie/"; // Url para acceder a todos los métodos del API
const baseImgUrl =
    "https://image.tmdb.org/t/p/"; // Url para acceder a las imagenes del API (debe proporcionarse tambien los tamaños)
const apiKey = "a4fa87e1b5f78c15281c36e3a8fc67dc"; // Key para acceder a la API
// Url's base para consumir datos del API
const nowPlayingUrl = "${baseUrl}now_playing?api_key=$apiKey";
const upcomingMoviesUrl = "${baseUrl}upcoming?api_key=$apiKey";
const popularMoviesUrl = "${baseUrl}popular?api_key=$apiKey";
const topRatedMoviesUrl = "${baseUrl}top_rated?api_key=$apiKey";

class MyMovieApp extends StatefulWidget {
  @override
// ? StatatlessWidget  -> Dibuja algo en la pantalla pero eso que dibuja no cambia en ningún momento por mas que el usuario haga cosas (estático)
// ? StateFulWidged    -> Puede menejar los cambios de estado dados por la interacción del usuario (dinámico)
  _MyMovieApp createState() => new _MyMovieApp();
}

// ? Objeto Estado
class _MyMovieApp extends State<MyMovieApp> {
  // Instancias de la clase Movie -> Almacena los resultados
  Movie nowPlayingMovies;
  Movie upcomingMovies;
  Movie popularMovies;
  Movie topRatedMovies;

  int heroTag = 0;

  // * Init State (Método de la clase State que me permite iniciar cosas antes de que se ejecute el Build)
  @override
  void initState() {
    super.initState();
    _fetchNowPlayingMovies();
    _fetchUpcomingMovies();
    _fetchPopularMovies();
    _fetchTopRatedMovies();
  }

  // * Como el método del _fetch es async la app va a correr el Build primero por lo cual debemos actualizar                                                   para mostrar los valores traidos de la API Para eso utilizamos el SET STATE()
  // Métodos para traerme las peliculas correspondientes (debe ser async para que no se quede esperando el resultado y continue su ejecución)
  void _fetchNowPlayingMovies() async {
    var response = await http.get(nowPlayingUrl);
    var decodeJson = json.decode(response.body);
    setState(() {
      nowPlayingMovies = Movie.fromJson(decodeJson);
      //print(nowPlayingMovies);
    });
  }

  void _fetchUpcomingMovies() async {
    var response = await http.get(upcomingMoviesUrl);
    var decodeJson = json.decode(response.body);
    setState(() {
      upcomingMovies = Movie.fromJson(decodeJson);
    });
  }

  void _fetchPopularMovies() async {
    var response = await http.get(popularMoviesUrl);
    var decodeJson = json.decode(response.body);
    setState(() {
      popularMovies = Movie.fromJson(decodeJson);
    });
  }

  void _fetchTopRatedMovies() async {
    var response = await http.get(topRatedMoviesUrl);
    var decodeJson = json.decode(response.body);
    setState(() {
      topRatedMovies = Movie.fromJson(decodeJson);
    });
  }

  // Método para construir el Carousel (retorna el widget)
  Widget _buildCarouselSlider() => CarouselSlider(
        items: nowPlayingMovies == null
            ? <Widget>[Center(child: CircularProgressIndicator())]
            : nowPlayingMovies.results
                .map((movieItem) => _buildMovieItem(movieItem)) //refactor
                .toList(), //result es un arreglo de la API que contiene las peliculas, en ese caso guardadas ya en 'nowPlayginMovies' y las recorro con Map
        autoPlay: false,
        height: 240.0,
        viewportFraction: 0.5,
      );

  // Listas de peliculas parecido al _buildCarouselSlider -> String para el titulo del container y movie para ver cual debo mostrar
  // Métodos para desplegar 'upcoming movies', 'popular movies' y 'top rated movies'

  Widget _buildMovieItem(Results movieItem) {
    heroTag += 1;
    return Material(
        elevation: 15.0,
        //Splash cuando se hace tab en un elemento
        child: InkWell(
            onTap: () {}, //P/ Navegar otras pantallas
            child: Hero(
              //Widget de animación entre pantallas
              tag: heroTag,
              child: Image.network("${baseImgUrl}w342${movieItem.posterPath}",
                  fit: BoxFit.cover),
            )));
  }

  Widget _buildMovieListItem(Results movieItem) => Material(
      // Recibe 'MovieItem => ' que es el Result del map
      child: Container(
          width: 128.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(6.0),
                  child: _buildMovieItem(movieItem)
              ),
              Padding(
                padding: EdgeInsets.only(left: 6.0, top: 2.0),
                child: Text(
                  movieItem.title,
                  style: TextStyle(fontSize: 8.0),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 6.0, top: 2.0),
                  child: Text(
                    movieItem.releaseDate,
                    style: TextStyle(fontSize: 8.0), 
                  )
              )
            ],
          )));

  Widget _buildMoviesListView(Movie _movie, String _movieListTitle) =>
      Container(
        height: 258.0,
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.4),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Titutlo
            Padding(
                padding: EdgeInsets.only(left: 7.0, bottom: 7.0),
                child: Text(_movieListTitle,
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[400]))),
            // Movie
            Flexible(
                child: ListView(
              scrollDirection: Axis.horizontal,
              children: _movie == null
                  ? <Widget>[Center(child: CircularProgressIndicator())]
                  : _movie.results.map((movieItem) => Padding(padding: EdgeInsets.only(left: 6.0, right: 2.0),
                    child:_buildMovieListItem(movieItem),  
                  )).toList(),
            ))
          ],
        ),
      );
      

  // ? Método BUILD que construye cuando se ejecuta la app (1) Barra principal del top de la app (+ iconos)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Movies App',
            style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.bold)),

        centerTitle: true,

        //  Icono del menu de labarra de arriba
        leading: IconButton(
          icon: Icon(Icons.menu), //icono
          onPressed: () {/* Barra principal del top de la app */},
        ),

        //  Podría tener varios widgets en 'actions' ya que viene como colección
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          // IconButton(
          //   icon: Icon(Icons.message),
          //   onPressed: (){},
          // ),
        ],
      ),

      //Carrousel y widgeds del body
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text('Now Playing',
                      style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold)),
                ),
              ),
              expandedHeight: 290.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: <Widget>[
                    Container(
                      child: Image.network(
                        "${baseImgUrl}w500/y95lQLnuNKdPAzw9F9Ab8kJ80c3.jpg",
                        fit: BoxFit.cover,
                        width: 1000.0,
                        colorBlendMode: BlendMode.dstATop, //img de fondo
                        color: Colors.blue.withOpacity(0.5),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: 35.0),
                        child: _buildCarouselSlider()),
                  ],
                ),
              ),
            )
          ];
        },
        //body: Text('Hola'),
        body: ListView(
          children: <Widget>[
            _buildMoviesListView(upcomingMovies, 'Pronto'),
            _buildMoviesListView(popularMovies, 'Populares'),
            _buildMoviesListView(topRatedMovies, 'Mejor Puntuadas'),
          ],
        ),
      ),
    );
  }
}
