import 'package:flutter/material.dart';
import 'mainClases.dart';

// ! https://www.themoviedb.org/settings/api

void main() => runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Movie App',
    theme: ThemeData.dark(),
    home: MyMovieApp()));
// ! Fin Main()  

